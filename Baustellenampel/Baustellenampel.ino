

int rechtsGruen = 2;  // linke Ampel: Digitalpin 2  wird rechtsGruen
int rechtsGelb = 3;   
int rechtsRot = 4;   

int linksGruen = 8;  // rechte Ampel: Digitalpin 8 wird rechtsGruen
int linksGelb = 7;
int linksRot = 5;

void setup() { // Definition der Pins für die Ausgabe:
  pinMode(rechtsGruen, OUTPUT); // setze Pin 2 auf Output
  pinMode(rechtsGelb, OUTPUT); //           3
  pinMode(rechtsRot, OUTPUT); //           4

  pinMode(linksGruen, OUTPUT); //           8
  pinMode(linksGelb, OUTPUT); //           7
  pinMode(linksRot, OUTPUT); //           5
}

void loop(){
  digitalWrite(rechtsRot, HIGH);   // 5V Spannung anlegen an 
  digitalWrite(linksGruen, HIGH);  // rechtsRot und linksGruen
  delay(5000);                     // 5 Sekunden warten
  blink(linksGruen, 4);             // linksGruen blinkt für 4 Sekunden 
  digitalWrite(linksGruen, LOW);   
  digitalWrite(linksGelb, HIGH);
  delay(1000);
  digitalWrite(linksGelb, LOW);
  digitalWrite(linksRot, HIGH);
  delay(1000);
  digitalWrite(rechtsGelb, HIGH);
  delay(1000);
  digitalWrite(rechtsRot, LOW);
  digitalWrite(rechtsGelb, LOW);
  digitalWrite(rechtsGruen, HIGH);
  delay(5000);
  blink(rechtsGruen, 4);
  digitalWrite(rechtsGruen, LOW);
  digitalWrite(rechtsGelb, HIGH);
  delay(1000);
  digitalWrite(rechtsGelb, LOW);
  digitalWrite(rechtsRot, HIGH);
  delay(1000);
  digitalWrite(linksGelb, HIGH);
  delay(1000);
  digitalWrite(linksRot, LOW);
  digitalWrite(linksGelb, LOW);
}

void blink(int pin, int times){         // Funktion mit bezeichner ‚blink‘
  for(int i = 0; i < times; i++){  // Schleife
    digitalWrite(pin, LOW);
    delay(500);
    digitalWrite(pin, HIGH);
    delay(500);
  }
}

